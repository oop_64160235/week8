package com.khongdet.week8;

public class Circle {
    private double r;

    public Circle(double r) {
        this.r = r;

    }

    public void circleArea() {
        double Circle_area = 3.14 * r * r;
        System.out.println("Area of Circle : " + Circle_area);
    }

    public void printPerimeter() {
        double Circle_area = 3.14 * r * r;
        double PerimeterCircleArea = 2 * Circle_area;
        System.out.println("Perimeter of Circle : " + PerimeterCircleArea);
    }
}
