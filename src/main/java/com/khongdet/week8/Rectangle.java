package com.khongdet.week8;

public class Rectangle {
    private int width, height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void printArea() {
        int Square_area = width * height;
        System.out.println("Area of Square : " + Square_area);

    }

    public void printPerimeter() {
        int Square_area = width * height;
        double rootSquare = (Math.sqrt(Square_area)) * 4;
        System.out.println("Perimeter of Square : " + rootSquare);
    }
}
